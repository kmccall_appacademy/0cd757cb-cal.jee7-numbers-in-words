require "byebug"

class Fixnum
  def in_words
    # byebug
    return "zero" if self == 0
    word_blocks = {
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine",
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen",
      20 => "twenty",
      30 => "thirty",
      40 => "forty",
      50 => "fifty",
      60 => "sixty",
      70 => "seventy",
      80 => "eighty",
      90 => "ninety"
    }

    words = []
    digits = self.to_s.chars

    n = 0
    until digits == []

      triplet = digits.pop(3)
      triplet_words = []

      singles = triplet[-1]
      tens = triplet[-2] || "0"
      hundreds = triplet[-3] || "0"

      if hundreds != "0"
        triplet_words << word_blocks[hundreds.to_i] + " hundred"
      end

      doublet = (tens + singles).to_i
      if doublet > 0 && doublet < 20
        triplet_words << word_blocks[(tens + singles).to_i]
      elsif doublet >= 20
        triplet_words << word_blocks[tens.to_i * 10]
        triplet_words << word_blocks[singles.to_i] if singles != "0"
      end

      if !triplet_words.empty?
        case n
        when 0
          words.unshift(triplet_words.join(" "))
        when 1
          triplet_words << "thousand"
          words.unshift(triplet_words.join(" "))
        when 2
          triplet_words << "million"
          words.unshift(triplet_words.join(" "))
        when 3
          triplet_words << "billion"
          words.unshift(triplet_words.join(" "))
        when 4
          triplet_words << "trillion"
          words.unshift(triplet_words.join(" "))
        end
      end

      n += 1
    end

    words.join(" ")
  end
end
